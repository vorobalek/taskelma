﻿using System;
using System.Collections.Generic;

namespace Liberdoc.Models
{
    public class Person : IComparable
    {
        private IList<Document> _documents;

        public virtual Guid Id { get; set; }
        public virtual string Username { get; set; }
        public virtual string Password { get; set; }
        public virtual IList<Document> Documents
        {
            get
            {
                return _documents ?? (_documents = new List<Document>());
            }
            set { _documents = value; }
        }

        int IComparable.CompareTo(object obj)
        {
            Person person = (Person)obj;
            return Username.CompareTo(person.Username);
        }
    }
}