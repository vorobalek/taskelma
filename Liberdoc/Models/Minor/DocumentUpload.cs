﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace Liberdoc.Models.Minor
{
    public class DocumentUpload
    {
        [StringLength(255, MinimumLength = 1, ErrorMessage = "Длина строки должна быть от 1 до 255 символов")]
        [DisplayName("Имя документа")]
        [Required(AllowEmptyStrings = false)]
        public string Name { get; set; }
    }
}