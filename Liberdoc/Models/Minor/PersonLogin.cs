﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace Liberdoc.Models.Minor
{
    public class PersonLogin
    {
        [StringLength(255, MinimumLength = 1, ErrorMessage = "Длина строки должна быть от 1 до 255 символов")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "Без логина не обойтись!")]
        [DisplayName("Логин")]
        public string Username { get; set; }

        [StringLength(255, MinimumLength = 1, ErrorMessage = "Длина строки должна быть от 1 до 255 символов")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "Без пароля не пройти!")]
        [DisplayName("Пароль")]
        [DataType(DataType.Password)]
        public string Password { get; set; }
    }
}