﻿using System;

namespace Liberdoc.Models
{
    public class Document
    {
        public virtual Guid Id { get; set; }
        public virtual string FileName { get; set; }
        public virtual string RealName { get; set; }
        public virtual string Path { get; set; }
        public virtual Person Author { get; set; }
        public virtual DateTime Date { get; set; }
        public virtual byte[] Bytes { get; set; }
    }
}