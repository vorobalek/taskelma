﻿using System;
using System.Collections.Generic;

namespace Liberdoc.Models.Repositoies
{
    interface IPersonRepository
    {
        Person GetByStringGuid(string stringGuid);
        IEnumerable<Person> GetByUsername(string username);
    }
}
