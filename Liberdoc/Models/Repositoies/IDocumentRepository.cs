﻿using System;
using System.Collections.Generic;

namespace Liberdoc.Models.Repositoies
{
    interface IDocumentRepository
    {
        void Add(Document document);
        Document GetByStringGuid(string stringGuid);
        ICollection<Document> GetOrderedBy(string criterion);
    }
}
