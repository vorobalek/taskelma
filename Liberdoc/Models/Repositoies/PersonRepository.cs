﻿using System.Collections.Generic;
using System.Linq;

namespace Liberdoc.Models.Repositoies
{
    public class PersonRepository : IPersonRepository
    {
        public Person GetByStringGuid(string stringGuid)
        {
            Person person;
            NHibernateHelper.OpenSession();
            using (NHibernateHelper.Session.BeginTransaction())
            {
                var criteria = NHibernateHelper.Session.CreateCriteria<Person>();
                person = criteria.List<Person>().FirstOrDefault(it => it.Id.ToString() == stringGuid);
            }
            return person;
        }

        public IEnumerable<Person> GetByUsername(string username)
        {
            IEnumerable<Person> persons;
            NHibernateHelper.OpenSession();
            using (NHibernateHelper.Session.BeginTransaction())
            {
                var criteria = NHibernateHelper.Session.CreateCriteria<Person>();
                persons = criteria.List<Person>().Where(it => it.Username == username);
            }
            return persons;
        }
    }
}