﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Liberdoc.Models.Repositoies
{
    public class DocumentRepository : IDocumentRepository
    {
        public void Add(Document document)
        {
            NHibernateHelper.OpenSession();
            using (var transaction = NHibernateHelper.Session.BeginTransaction())
            {
                NHibernateHelper.Session.Save(document);
                transaction.Commit();
            }
        }

        public Document GetByStringGuid(string stringGuid)
        {
            NHibernateHelper.OpenSession();
            Document document;
            using (NHibernateHelper.Session.BeginTransaction())
            {
                var criteria = NHibernateHelper.Session.CreateCriteria<Document>();
                var documents = criteria.List<Document>();
                document = documents.FirstOrDefault(it => it.Id.ToString() == stringGuid);
            }
            return document;
        }

        public ICollection<Document> GetOrderedBy(string criterion)
        {
            NHibernateHelper.OpenSession();
            IList<Document> documents;
            using (NHibernateHelper.Session.BeginTransaction())
            {
                var criteria = NHibernateHelper.Session.CreateCriteria<Document>();
                documents = criteria
                            .AddOrder(NHibernate.Criterion.Order.Asc(criterion ?? "FileName"))
                            .List<Document>();
            }
            return documents;
        }
    }
}