﻿using NHibernate;
using NHibernate.Cfg;
using NHibernate.Cfg.MappingSchema;
using NHibernate.Dialect;
using NHibernate.Mapping.ByCode;
using NHibernate.Tool.hbm2ddl;
using System.Reflection;

namespace Liberdoc
{
    public class NHibernateHelper
    {
        public static ISession Session { get; private set; }

        public static void OpenSession()
        {
            if (Session != null && Session.IsOpen)
            {
                Session.Close();
            }

            var cfg = new Configuration()
            .DataBaseIntegration(db => {
                db.ConnectionString = @"Server=localhost;initial catalog=Liberdoc;Integrated Security=SSPI;";
                db.Dialect<MsSql2012Dialect>();
            });
            var mapper = new ModelMapper();
            mapper.AddMappings(Assembly.GetExecutingAssembly().GetExportedTypes());
            HbmMapping mapping = mapper.CompileMappingForAllExplicitlyAddedEntities();
            cfg.AddMapping(mapping);
            new SchemaUpdate(cfg).Execute(true, true);
            ISessionFactory sessionFactory = cfg.BuildSessionFactory();
            Session = sessionFactory.OpenSession();
        }
    }
}