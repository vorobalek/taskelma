﻿using Liberdoc.Models;
using Liberdoc.Models.Minor;
using Liberdoc.Models.Repositoies;
using System.Linq;
using System.Web.Mvc;
using System.Web.Security;

namespace Liberdoc.Controllers
{
    public class AccountController : Controller
    {
        public ActionResult Login()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Login(PersonLogin model, string returnUrl)
        {
            string decodedUrl = "";
            if (!string.IsNullOrEmpty(returnUrl))
                decodedUrl = Server.UrlDecode(returnUrl);

            if (ModelState.IsValid)
            {
                var repository = new PersonRepository();
                var person = repository.GetByUsername(model.Username)
                    .FirstOrDefault(it => it.Password == model.Password);
                if (person != null)
                {
                    FormsAuthentication.SetAuthCookie(person.Id.ToString(), true);
                    if (Url.IsLocalUrl(decodedUrl))
                    {
                        return Redirect(decodedUrl);
                    }
                    else
                    {
                        return RedirectToAction("Index", "Home");
                    }
                }
                ModelState.AddModelError("", "Неверный логин или пароль!");
            }
            return View(model);
        }

        [Authorize]
        public ActionResult Logout()
        {
            FormsAuthentication.SignOut();
            return RedirectToAction("Index", "Home");
        }
    }
}