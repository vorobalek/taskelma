﻿using Liberdoc.Models;
using Liberdoc.Models.Minor;
using Liberdoc.Models.Repositoies;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Liberdoc.Controllers
{
    [Authorize]
    public class DocumentsController : Controller
    {
        public ActionResult Index()
        {
            var documentRepository = new DocumentRepository();
            ViewBag.Documents = documentRepository.GetOrderedBy("FileName");
            return View();
        }

        public ActionResult Upload()
        {
            return RedirectToAction("Index");
        }

        [HttpGet]
        public FileResult Download(string file_id)
        {
            var documentRepository = new DocumentRepository();
            var document = documentRepository.GetByStringGuid(file_id);
            if (document != null)
            {
                if (!System.IO.File.Exists(document.Path + document.Id.ToString()))
                {
                    System.IO.File.WriteAllBytes(document.Path + document.Id.ToString(), document.Bytes);
                }
                return File(document.Path + document.Id.ToString(), "application/octet-stream", document.RealName);
            }
            return null;
        }

        [HttpPost]
        public ActionResult UploadResult(DocumentUpload model, HttpPostedFileBase upload)
        {
            var result = new DocumentUploadResult();
            if (upload != null)
            {
                string fileName = Path.GetFileName(upload.FileName);
                if (!Directory.Exists(Server.MapPath("~/Files/")))
                {
                    Directory.CreateDirectory(Server.MapPath("~/Files/"));
                }
                if (ModelState.IsValid)
                {
                    var personRepository = new PersonRepository();
                    Person person = personRepository.GetByStringGuid(User.Identity.Name.ToString());
                    Document document = new Document()
                    {
                        Id = new Guid(),
                        FileName = model.Name,
                        RealName = fileName,
                        Path = Server.MapPath("~/Files/"),
                        Date = DateTime.Now,
                        Author = person,
                        Bytes = new byte[upload.InputStream.Length]
                    };
                    upload.InputStream.Read(document.Bytes, 0, (int)upload.InputStream.Length);
                    upload.SaveAs(Server.MapPath($"~/Files/{document.Id.ToString()}"));

                    var documentRepositiry = new DocumentRepository();
                    documentRepositiry.Add(document);

                    result.Result = $"Файл {fileName} успешно загружен под именем \"{model.Name}\"";
                }
                else
                {
                    result.Result = "Что-то пошло не так, мы уже разбираемся";
                }
            }
            else
            {
                result.Result = "Не был выбран файл!";
            }
            return View(result);
        }

        public ActionResult Find()
        {
            return RedirectToAction("Index");
        }

        public ActionResult Order(string orderby)
        {
            var documentRepository = new DocumentRepository();
            var documents = documentRepository.GetOrderedBy(orderby);
            
            return PartialView(documents);
        }
    }
}