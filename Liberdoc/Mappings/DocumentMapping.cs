﻿using Liberdoc.Models;
using NHibernate;
using NHibernate.Mapping.ByCode;
using NHibernate.Mapping.ByCode.Conformist;

namespace Liberdoc.Mappings
{
    public class DocumentMapping : ClassMapping<Document>
    {
        public DocumentMapping()
        {
            Id(it => it.Id, 
                map => map.Generator(Generators.Guid));
            Property(it => it.FileName);
            Property(it => it.RealName);
            Property(it => it.Path);
            Property(it => it.Date);
            Property(it => it.Bytes, map =>
            {
                map.Type(NHibernateUtil.BinaryBlob);
                map.Length(int.MaxValue);
            });
            ManyToOne(it => it.Author,
                map => { map.Cascade(Cascade.Persist); map.Column("AuthorId"); });
        }
    }
}