﻿using Liberdoc.Models;
using NHibernate.Mapping.ByCode;
using NHibernate.Mapping.ByCode.Conformist;

namespace Liberdoc.Mappings
{
    public class PersonMapping : ClassMapping<Person>
    {
        public PersonMapping()
        {
            Id(it => it.Id, 
                map => map.Generator(Generators.Guid));
            Property(it => it.Username);
            Property(it => it.Password);
            Bag(it => it.Documents,
                collect => { collect.Key(key => key.Column("AuthorId")); collect.Inverse(true); },
                map => map.OneToMany());
        }
    }
}